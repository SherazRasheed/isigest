//
//  ApplicationNavigationBar.swift
//  Lung Direct
//
//  Created by Onebyte LLC on 6/30/17.
//  Copyright © 2017 Onebyte LLC. All rights reserved.
//

import UIKit
import Foundation

extension Date {
    //MARK: Applicants Date/Time formating
    func getApplicantsDateFormatString() -> String{
        let applicantsDateFormatter = DateFormatter()
        applicantsDateFormatter.dateFormat = "d MMM yyyy"
        
        return applicantsDateFormatter.string(from: self)
    }
    
    func getApplicantsTimeFormatedString() -> String {
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "h:mm a"
        let date = self
        let stringOfDate = dateFormate.string(from: date as Date)
        return stringOfDate
    }
    
    //MARKL Applicants Date/Time Formatting
    func getJobApplicantsServerDateFormatedString() -> String {
        let dateFormate = DateFormatter()
        //'T'HH:mm:ss.SSS'Z'
        dateFormate.dateFormat = "yyyy-MM-dd"
        let date = self
        let stringOfDate = dateFormate.string(from: date as Date)
        return stringOfDate
    }

    
    
    //MARK: Job Schedule Date/Time formating
    func getJobScheduleMonthFormatString() -> String{
        let applicantsDateFormatter = DateFormatter()
        applicantsDateFormatter.dateFormat = "MM"
        
        return applicantsDateFormatter.string(from: self)
    }
    
    func getJobScheduleMonthAndYearFormatString() -> String{
        let applicantsDateFormatter = DateFormatter()
        applicantsDateFormatter.dateFormat = "MMMM yyyy"
        
        return applicantsDateFormatter.string(from: self)
    }
    

    func getJobScheduleYearFormatString() -> String{
        let applicantsDateFormatter = DateFormatter()
        applicantsDateFormatter.dateFormat = "yyyy"
        
        return applicantsDateFormatter.string(from: self)
    }
    func getJobScheduleDateFormatString() -> String{
        let applicantsDateFormatter = DateFormatter()
        applicantsDateFormatter.dateFormat = "dd.MM.yyyy"
        
        return applicantsDateFormatter.string(from: self)
    }
    
    func getJobScheduleTimeFormatedString() -> String {
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = "h:mm a"
        let date = self
        let stringOfDate = dateFormate.string(from: date as Date)
        return stringOfDate
    }
    
    func getJobScheduleServerDateFormatedString() -> String {
        let dateFormate = DateFormatter()
        //'T'HH:mm:ss.SSS'Z'
        dateFormate.dateFormat = "yyyy-MM-dd"
        let date = self
        let stringOfDate = dateFormate.string(from: date as Date)
        return stringOfDate
    }
    func getJobScheduleServerTimeFormatedString() -> String {
        let dateFormate = DateFormatter()
        
        dateFormate.dateFormat = "HH:mm:ss"
        let date = self
        let stringOfDate = dateFormate.string(from: date as Date)
        return stringOfDate
    }
    
    func getJobScheduleServerDateTimeFormatedString() -> String {
        let dateFormate = DateFormatter()
        
        dateFormate.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date = self
        let stringOfDate = dateFormate.string(from: date as Date)
        return stringOfDate
    }
    
}
