//
//  UIImageView+Helpers.swift
//  MySwiftApplication
//
//  Created by OneByte on 10/5/17.
//  Copyright © 2017 iDevz. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

extension UIImageView {
    func addGradientLayer(frame: CGRect){
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = [UIColor.black.cgColor, UIColor.red.cgColor]
        gradient.locations = [0.0, 1]
        self.layer.addSublayer(gradient)
    }
    
    public func maskCircle(anyImage: UIImage) {
        self.contentMode = UIViewContentMode.scaleAspectFill
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
        self.image = anyImage
    }
}
