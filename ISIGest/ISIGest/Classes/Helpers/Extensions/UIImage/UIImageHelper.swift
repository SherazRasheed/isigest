//
//  UIImageHelper.swift
//  JobsNow
//
//  Created by Abdul Sami on 31/10/2017.
//  Copyright © 2017 iDevz. All rights reserved.
//

import Foundation
import UIKit

extension UIImage{
    
    public class func selectedTabBackgroundImage (width: CGFloat, height: CGFloat) -> UIImage{
        let size: CGSize = CGSize(width: width, height: height)
        let layer: CAGradientLayer = CAGradientLayer()
        
        layer.frame = CGRect(x: 0, y: 0, width: width, height: height)

        layer.colors = [UIColor.tabBarGradientStartColor().cgColor, UIColor.tabBarGradientEndColor().cgColor]
        
        layer.locations = [0, 1]
        layer.startPoint = CGPoint(x: 0.76, y: 0.82)
        layer.endPoint = CGPoint(x: 0.76, y: -1.53)
        
        UIGraphicsBeginImageContext(size)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)

        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return image
    }
}
