//
//  UIScreen+JNHelper.swift
//  JobsNow
//
//  Created by Abdul Sami on 08/11/2017.
//  Copyright © 2017 iDevz. All rights reserved.
//

import Foundation
import UIKit

extension UIScreen {
    
    public class func mainScreenWidth () -> CGFloat {
        return UIScreen.main.bounds.width
    }
    
    public class func mainScreenHeight () -> CGFloat {
        return UIScreen.main.bounds.height
    }
}
