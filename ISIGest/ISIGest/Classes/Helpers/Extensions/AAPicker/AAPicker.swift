//
//  AAPicker.swift
//  expomobile-swift
//
//  Created by Onebyte LLC on 19/06/2017.
//  Copyright © 2017 Onebyte LLC. All rights reserved.
//

//import UIKit
//
//extension UIViewController {
//
//    func picker(){
//    var customPickerView: CustomView = {
//        let view = CustomView(pickerType: .singleComponent, items: [
//            ("00", "Row 0")
//            , ("02", "Row 1")
//            , ("04", "Row 2")
//            , ("06", "Row 3")
//            , ("09", "Row 4")
//            , ("11", "Row 5")
//            ])
//        // MARK: - Settings
//        view.blackBackground = true
//        view.isTitleBarHidden = false
//        view.isTapBackgroundEnabled = true
//        view.leftButton.setTitle("LEFT", for: .normal)
//        view.rightButton.setTitle("RIGHT", for: .normal)
//        view.title.text = "TITLE"
//        view.foregroundView.picker.backgroundColor = UIColor.white
//        view.foregroundView.bottomDivider.isHidden = true
//        // MARK: - Handler
//        view.setItemConfigHandler({ (item) -> String in
//            return item.content
//        })
//        view.setLeftButtoTapHandler({ [weak self] (view, chosenIndex, chosenItem) -> (shouldHide: Bool, animated: Bool) in
//            let hide = true
//            let animated = true
//            //            self?.logLabel.text = "Did Tap Left Button.\n <Index: \(chosenIndex)> \n<chosenItem: \(chosenItem)> \n<Hide: \(hide)> \n<Animated: \(animated)>"
//            //            print(self?.logLabel.text ?? "")
//            return (hide, animated)
//        })
//        view.setRightButtoTapHandler({ [weak self] (view, chosenIndex, chosenItem) -> (shouldHide: Bool, animated: Bool) in
//            let hide = true
//            let animated = true
//            //            self?.logLabel.text = "Did Tap Right Button. \n<Index: \(chosenIndex)> \n<chosenItem: \(chosenItem)> \n<Hide: \(hide)> \n<Animated: \(animated)>"
//            //            print(self?.logLabel.text ?? "")
//            return (hide, animated)
//        })
//        view.setDidScrollHandler({ [weak self] (view, chosenIndex, chosenItem) -> (shouldHide: Bool, animated: Bool) in
//            let hide = false
//            let animated = false
//            //            self?.logLabel.text = "Did Scroll. \n<Index: \(chosenIndex)> \n<chosenItem: \(chosenItem)> \n<Hide: \(hide)> \n<Animated: \(animated)>"
//            //            print(self?.logLabel.text ?? "")
//            return (hide, animated)
//        })
//        view.setWillShowHandler({ [weak self] (view) in
//            //            self?.showStatus.text = "View Will Show"
//            //            print(self?.showStatus.text ?? "")
//        })
//        view.setDidShowHandler({ [weak self] (view) in
//            //            self?.showStatus.text = "View Did Show"
//            //            print(self?.showStatus.text ?? "")
//        })
//        view.setWillHideHandler({ [weak self] (view) in
//            //            self?.showStatus.text = "View Will Hide"
//            //            print(self?.showStatus.text ?? "")
//        })
//        view.setDidHideHandler({ [weak self] (view) in
//            //            self?.showStatus.text = "View Did Hide"
//            //            print(self?.showStatus.text ?? "")
//        })
//        return view
//    }()
//    }
//}
