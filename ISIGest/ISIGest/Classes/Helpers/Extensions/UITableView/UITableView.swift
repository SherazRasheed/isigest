//
//  UITableView.swift
//  JobsNow
//
//  Created by Shery on 26/12/2017.
//  Copyright © 2017 iDevz. All rights reserved.
//

import UIKit

extension UITableView {
    func scrollToBottom(animated: Bool) {
        let y = contentSize.height - frame.size.height
        setContentOffset(CGPoint(x: 0, y: (y<0) ? 0 : y), animated: animated)
    }
}

