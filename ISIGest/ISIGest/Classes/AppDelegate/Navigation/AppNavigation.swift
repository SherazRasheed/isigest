//
//  AppNavigation.swift
//  JobsNow
//
//  Created by Onebyte on 10/23/17.
//  Copyright © 2017 iDevz. All rights reserved.
//

import Foundation
import UIKit

extension AppDelegate{
    func moveToLogin(){
        let navigationController : UINavigationController = UINavigationController(rootViewController: LoginViewController(nibName: "LoginViewController", bundle: nil))
        
        window?.rootViewController = navigationController
    }
    
    func moveToLoginPassword(){
    }
}
